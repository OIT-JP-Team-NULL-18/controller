#include "Controller.h"



Controller::Controller()
{
	_route = -1;
	socket_led.setup_server(mysettings.get_led_socket().c_str());
	socket_gps.setup_server(mysettings.get_gps_socket().c_str());
	socket_lcd.setup_server(mysettings.get_lcd_socket().c_str());
	socket_bell.setup_server(mysettings.get_bell_socket().c_str());
	
	socket_led.server_accept(5);
	socket_gps.server_accept(5);
	socket_lcd.server_accept(5);
	socket_bell.server_accept(5);
}


Controller::~Controller()
{
}

void Controller::Run()
{
	RunRoute(Idle());
}

void Controller::RunRoute(int routenum)
{
	bool skip_stop = false;
	bool manual = false;
	bool inside = false;
	bool bellpull = false;
	stoplist = database.GetRoute(routenum);
	if(stoplist.empty())
	{
		throw(Exception("Route entered is invalid"));
	}
	while (!stoplist.empty())
	{
		bool next = false;
		Coordinate stop = stoplist.front().gps.Location;
		char Routebuff[100];
		sprintf(Routebuff, "%d", routenum);
		if(socket_lcd.connected()) //send route and stop info to lcd
		{
				string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":0:";
				char * msg = strdup(temp.c_str());
				socket_lcd.send_msg((void*) msg, strlen(msg));
				delete msg;
		}
		if(socket_led.connected()) //send route and stop info to led
		{

				string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":0:";
				char * msg = strdup(temp.c_str());
				socket_led.send_msg((void*) msg, strlen(msg));
				delete msg;
		}
		
		while(next == false && skip_stop == false)
		{
			while (inside == false && mylcd.manual == false && skip_stop == false)
			{
				int threshold = stoplist.front().stopthreshold;
				if(threshold < mysettings.get_threshold_min()) threshold = mysettings.get_threshold_min();
				bool check = CheckGPS(stop, threshold);
				if (check)
				{
					inside = true;
					string command = "espeak \"approaching " + stoplist.front().Name + '\"';
					system(command.c_str());
				}
				///check if now in manual mode.
				LCDgetdata();
				skip_stop = mylcd.skipstop;
				if(socket_bell.connected()&& bellpull == false) 
				{

					if(socket_bell.data_avail() || mylcd.requeststop == true)
					{		
						bellpull = true;
						mylcd.requeststop = false;
						if(socket_lcd.connected())
						{
							string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":1:";
							char * msg = strdup(temp.c_str());
							socket_lcd.send_msg((void*) msg, strlen(msg));
							delete msg;
						}
						if(socket_led.connected()) 
						{
							string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":1:";
							char * msg = strdup(temp.c_str());
							socket_led.send_msg((void*) msg, strlen(msg));
							delete msg;
						}
						string command = "espeak \"Stop requested for " + stoplist.front().Name + '\"';
						system(command.c_str());
					}
				}
			}
			while (inside == true && mylcd.manual == false && skip_stop == false)
			{
				int temp = (stoplist.front().stopthreshold) * (mysettings.get_threshold_percent() / 100.0);
				bool check = CheckGPS(stop, temp);
				if (check)
				{
					string command = "espeak \"Now arriving at " + stoplist.front().Name + '\"';
					system(command.c_str());
					stoplist.pop_front();
					inside = false;
					next = true;

				}
				///check if in manual mode.
				LCDgetdata();
				skip_stop = mylcd.skipstop;
			
				if(socket_bell.connected() && inside == true && bellpull == false) 
				{
					if(socket_bell.data_avail() || mylcd.requeststop == true)
					{	
						bellpull = true;
						mylcd.requeststop = false;
						if(socket_lcd.connected())
						{
							string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":1:";
							char * msg = strdup(temp.c_str());
							socket_lcd.send_msg((void*) msg, strlen(msg));
							delete msg;
						}
						if(socket_led.connected()) 
						{
							string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":1:";
							char * msg = strdup(temp.c_str());
							socket_led.send_msg((void*) msg, strlen(msg));
							delete msg;
						}
						string command = "espeak \"Stop requested for " + stoplist.front().Name + '\"';
						system(command.c_str());
					}
				}		
			}
			while(mylcd.manual == true)
			{
				LCDgetdata();
				if(mylcd.announce == true)
				{
					mylcd.announce = false;
					string command = "espeak \"Now arriving at " + stoplist.front().Name + '\"';
					system(command.c_str());
				}
				if(mylcd.skipstop == true)
				{
					stoplist.pop_front();
					mylcd.skipstop = false;
					bellpull = false;
					next = true;
					if(!stoplist.empty())
					{
						if(socket_lcd.connected()) //send route and stop info to lcd
						{
							string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":0:";
							char * msg = strdup(temp.c_str());
							socket_lcd.send_msg((void*) msg, strlen(msg));
							delete msg;
						}
						if(socket_led.connected()) //send route and stop info to led
						{

							string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":0:";
							char * msg = strdup(temp.c_str());
							socket_led.send_msg((void*) msg, strlen(msg));
							delete msg;
						}
					
					}
					else
					{
						mylcd.manual = false;
					}
				}
				if(mylcd.requeststop == true && bellpull == false)
				{	
					bellpull = true;
					mylcd.requeststop = false;
					if(socket_lcd.connected())
					{
						string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":1:";
						char * msg = strdup(temp.c_str());
						socket_lcd.send_msg((void*) msg, strlen(msg));
						delete msg;
					}
					if(socket_led.connected()) 
					{
						string temp = string(Routebuff) + ':' + string(stoplist.front().CreateString()) + ":1:";
						char * msg = strdup(temp.c_str());
						socket_led.send_msg((void*) msg, strlen(msg));
						delete msg;
					}
					string command = "espeak \"Stop requested for " + stoplist.front().Name + '\"';
					system(command.c_str());
				}
			}
		}
		if(socket_bell.data_avail())
		{
		    delete (char *)socket_bell.receive_msg(5000);;
		}
		bellpull = false;
		if(skip_stop)
		{
			stoplist.pop_front();
			skip_stop = false;
		}
		mylcd.skipstop = false;
		mylcd.requeststop = false;
	}
	string command = "espeak \"Route is complete\"";
	system(command.c_str());
}
void Controller::LCDgetdata()
{

	if(socket_lcd.data_avail())	
	{
		char * garbage = (char*)socket_lcd.receive_msg(50);
		printf("%s\n",garbage);
		mylcd = LCD(garbage);
		delete garbage;
	}

	
}
bool Controller::CheckGPS(Coordinate stop, int threshold)
{
	bool withinthreshold = false;
	UpdateGPS();
	Coordinate c2 = CurrentGPS.Location;
	double distance = HaversineDistanceFeet(stop, c2);
	printf("%f\n",distance);
	if (distance <= threshold)
	{
		withinthreshold = true;
	}
	return withinthreshold;
}

int Controller::Idle()
{	
	while (_route == -1)
	{
		_route = RouteEnter();
	}
	return _route;
}

int Controller::RouteEnter()
{
	LCDgetdata();
	
	return mylcd.routenum;
}
void Controller::UpdateGPS() //able to be finished
{
	char * buffer = nullptr;
	
	socket_gps.send_msg((void*) "1\0", 2);
	usleep(500);
	buffer = (char*) socket_gps.receive_msg(1000);//get latest gps data
	CurrentGPS = GPS(buffer);
	printf("latitude is %f\n" , CurrentGPS.Location.Latitude);
    printf("longitude is %f\n" , CurrentGPS.Location.Longitude);
	delete buffer;
}
double Controller::HaversineDistance(const Coordinate & p1, const Coordinate & p2)
{
	
	double latRad1 = DegreeToRadian(p1.Latitude);
	double latRad2 = DegreeToRadian(p2.Latitude);
	double lonRad1 = DegreeToRadian(p1.Longitude);
	double lonRad2 = DegreeToRadian(p2.Longitude);

	double diffLa = latRad2 - latRad1;
	double doffLo = lonRad2 - lonRad1;

	double computation = asin(sqrt(sin(diffLa / 2) * sin(diffLa / 2) + cos(latRad1) * cos(latRad2) * sin(doffLo / 2) * sin(doffLo / 2)));
	return 2 * EarthRadiusKm * computation;
}

double Controller::HaversineDistanceFeet(const Coordinate & p1, const Coordinate & p2)
{
	return HaversineDistance(p1, p2) * 3280.8;
}
double Controller::DegreeToRadian(double angle)
{
	return M_PI * angle / 180.0;
}

