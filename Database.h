#pragma once
#include <list>
#include <fstream>
#include "common/Stop.hpp"
#include "common/Exception.h"
using std::list;
class Database
{
public:
	Database();
	~Database();
	list<Stop> GetRoute(int routenum);
private:

	void GetRouteStops(int routenum);
  Stop ParseGTFStoStop(char * stop);
  void ParseRoute(char * route);

  /*TODO: Move to std::string to contain StopID*/
	list<std::string> StopIDList;
	list<Stop> CurrentRoute;
};

