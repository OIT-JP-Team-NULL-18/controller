include common/common.mk

SOURCE_FILES += Database.cpp
SOURCE_FILES += Controller.cpp
SOURCE_FILES += main.cpp

DEBUG = -ggdb
WARNINGS = -Wall
CC = g++

all: controller

controller: main.cpp Controller.cpp Database.cpp
	$(CC) $(WARNINGS) $(DEBUG) $(SOURCE_FILES) -o $@

run: all
	./controller

clean:
	rm ./controller
