#pragma once
#define _USE_MATH_DEFINES
#include <list>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include "Database.h"
#include "common/common.h"

using std::list;
const static double EarthRadiusKm = 6372.8;
class Controller
{
public:
	Controller();
	~Controller();
	void Run();	
private:
	double HaversineDistance(const Coordinate& p1, const Coordinate& p2);
	double HaversineDistanceFeet(const Coordinate & p1, const Coordinate & p2);
	double DegreeToRadian(double angle);
	int Idle();
	void RunRoute(int routenum);
	bool CheckGPS(Coordinate stop, int threshold);
	int RouteEnter();
	void UpdateGPS();
	void LCDgetdata();

	list<Stop> stoplist;
	Database database;
	GPS CurrentGPS;
	int _route;


	settings mysettings;
	socket_t socket_gps;
	socket_t socket_bell;
	socket_t socket_led;
	socket_t socket_lcd;
	
	LCD mylcd;

};
