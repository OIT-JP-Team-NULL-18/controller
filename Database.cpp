#include "Database.h"



Database::Database()
{
}


Database::~Database()
{
}

list<Stop> Database::GetRoute(int routenum)
{

	GetRouteStops(routenum);
	return CurrentRoute;
}



Stop Database::ParseGTFStoStop(char * stop)
{
    Stop RetVal;
    RetVal.StopID = strtok(stop, ",");
    /*for (int idx = 0; idx < 2; ++idx)
    {
        strtok(nullptr, ",");
    }*/
    RetVal.Name = strtok(nullptr, ",");
    //strtok(nullptr, ",");
    RetVal.gps.Location.Latitude = atof(strtok(nullptr, ","));
    RetVal.gps.Location.Longitude = atof(strtok(nullptr, ","));
    return RetVal;
}

void Database::ParseRoute(char * route)
{
    char * temp = strtok(route, ",");
    StopIDList.push_front(string(temp));
    while(temp != nullptr)
    {
        temp = strtok(nullptr, ",");
        if(temp)
        {
            StopIDList.push_back(std::string(temp));
        }
    }
}

void Database::GetRouteStops(int routenum)
{
    bool flag = false;
	std::fstream stopFile("stops.txt");
	std::list<Stop> StopListFromFile;
	std::string line;
	std::string token;
	if(!(stopFile.is_open()))
	{
	    throw Exception("Stop file non existant");
	}
    char * line_cstr = nullptr;
    Stop temp;
	while(!stopFile.eof())
	{

	    getline(stopFile, line);
	    if (line != "\n" && line != "")
	    {
	        line_cstr = strdup(line.c_str());
	        temp = ParseGTFStoStop(line_cstr);
            StopListFromFile.push_back(temp);
            delete line_cstr;
            line_cstr = nullptr;
        }
	}
	stopFile.close();
	std::fstream routeFile("routes.txt");
	while(!routeFile.eof() && flag == false)
	{
	    getline(routeFile, line, ':');
	    if(routenum == atoi(line.c_str()))
	    {
	        getline(routeFile, line);
	        line_cstr = strdup(line.c_str());
	        ParseRoute(line_cstr);
	        delete line_cstr;
	        line_cstr = nullptr;
		flag = true;
	        
	    }
	    else
	    {
		getline(routeFile, line);
	    }
	}
    routeFile.close();
    if (flag == true)
    {
	for(std::list<std::string>::iterator IItr = StopIDList.begin(); IItr != StopIDList.end(); IItr++)
	{
	    for(std::list<Stop>::iterator Itr = StopListFromFile.begin(); Itr != StopListFromFile.end(); Itr++)
	    {
            if (Itr->StopID == *IItr)
            {
                CurrentRoute.push_back(*Itr);
            }
	    }
	}
    }
}
